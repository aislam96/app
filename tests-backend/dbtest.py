import datetime
import pytest
import requests
import json

dbURL = 'http://localhost:8070/api/'
# dbURL = 'https://ai-yb-prototype.appspot.com/api/'

uidA = "uidA"
uidB = "uidB"
uidC = "uidC"

postA1 = "postA1"
postB1 = "postB1"
postC1 = "postC1"
    
## friend relations: SENDER - RECIEVER. 
## A - A, A - B, B - A, B - B, C - A
## if the pair X - Y and Y - X exist then they are friends.
## field required: 
## MyUID (user ID of the one adding), FriendUID (ID of user being added)
def test_addfriend():
    addfriendURL = dbURL + 'addfriend'
    friendData = []
    friendData.append({
        "MyUID": uidA,
        "FriendUID": uidA
    })
    friendData.append({
        "MyUID": uidA,
        "FriendUID": uidB
    })
    friendData.append({
        "MyUID": uidB,
        "FriendUID": uidA
    })
    friendData.append({
        "MyUID": uidB,
        "FriendUID": uidB
    })    
    friendData.append({
        "MyUID": uidC,
        "FriendUID": uidA
    })

    for relation in friendData:
        resp = requests.post(addfriendURL, json=relation)
        assert resp.status_code == 200
    
    duplicateData = {
        "MyUID": uidA,
        "FriendUID": uidA
    }
    resp = requests.post(addfriendURL, json=duplicateData)
    assert resp.status_code == 500

def test_checkfriend():
    checkfriendURL = dbURL + 'checkfriend'
    friendDataMutual = []
    friendDataMutual.append({
        "MyUID": uidA,
        "FriendUID": uidA
    })
    friendDataMutual.append({
        "MyUID": uidA,
        "FriendUID": uidB
    })
    friendDataMutual.append({
        "MyUID": uidB,
        "FriendUID": uidA
    })
    friendDataMutual.append({
        "MyUID": uidB,
        "FriendUID": uidB
    })    

    for relation in friendDataMutual:
        resp = requests.post(checkfriendURL, json=relation)
        data = resp.json()
        assert resp.status_code == 200
        assert data.get('AddedMe', False)
        assert data.get('AddedByMe', False)

    ## Note: C added A however A did not add C back.
    friendDataExclusiveC = {
        "MyUID": uidC,
        "FriendUID": uidA
    }

    friendDataExclusiveA = {
        "MyUID": uidA,
        "FriendUID": uidC
    }

    resp = requests.post(checkfriendURL, json=friendDataExclusiveC)
    data = resp.json()
    assert resp.status_code == 200
    assert not data.get('AddedMe', True)
    assert data.get('AddedByMe', False)

    resp = requests.post(checkfriendURL, json=friendDataExclusiveA)
    data = resp.json()
    assert resp.status_code == 200
    assert data.get('AddedMe', True)
    assert not data.get('AddedByMe', False)    

# fields required for post request: 
# ID (post id), MyUID (poster's id), Time (time posted).
def test_addpost():
    addpostURL = dbURL + 'addpost'
    posts = []
    posts.append({
        "ID": postA1,
        "MyUID": uidA,
        "Time": datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    })
    posts.append({
        "ID": postB1,
        "MyUID": uidB,
        "Time": datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    })
    posts.append({
        "ID": postC1,
        "MyUID": uidC,
        "Time": datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    })
    for post in posts:
        resp = requests.post(addpostURL, json=post)
        assert resp.status_code == 200

    duplicatePost = {
        "ID": postC1,
        "MyUID": uidC,
        "Time": datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    }
    resp = requests.post(addpostURL, json=duplicatePost)
    assert resp.status_code == 500

# fields required for post request:
# MyUID (poster's ID), Offset (get 100 posts in starting at n*100 sorted by time)
def test_getposts():
    getpostURL = dbURL + 'getposts'
    
    postA = {
        "MyUID": uidA,
        "Offset": 0
    }
    postB = {
        "MyUID": uidB,
        "Offset": 1
    }
    postC = {
        "MyUID": uidC,
        "Offset": 0
    }

    resp = requests.post(getpostURL, json=postA)
    assert resp.status_code == 200
    data = resp.json()
    assert len(data) == 2

    resp = requests.post(getpostURL, json=postB)
    assert resp.status_code == 200
    data = resp.json()
    assert len(data) == 0

    resp = requests.post(getpostURL, json=postC)
    assert resp.status_code == 200
    data = resp.json()
    assert len(data) == 0

