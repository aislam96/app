import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'welcome',
      component: () => import('./views/Welcome.vue'),
    },
    {
        path: '/dashboard',
        component: () => import('./views/Dashboard.vue'),
        children: [
            {
                path: '',
                name: 'home',
                component: () => import('./components/dashboard/home/PostContainer.vue'),
            },
            {
                path: 'messages',
                name: 'messages',
                component: () => import('./components/dashboard/messages/Messages.vue'),
            },
            {
                path: 'search',
                name: 'search',
                component: () => import('./components/dashboard/search/Search.vue'),
            },
            {
                path: 'profile',
                component: () => import('./components/dashboard/profile/Profile.vue'),
                children: [
                    {
                        path: '',
                        name: 'profile',
                        component: () => import('./components/dashboard/profile/posts/PostContainer.vue'),
                    },
                    {
                        path: 'media',
                        name: 'media',
                        component: () => import('./components/dashboard/profile/media/MediaContainer.vue'),
                    },
                    {
                        path: 'friends',
                        name: 'friends',
                        component: () => import('./components/dashboard/profile/media/MediaContainer.vue'),
                    },
                ],
            },
        ],
    },
    {
        path: '/settings',
        component: () => import('./views/Settings.vue'),
        children: [
            {
                path: '',
                name: 'settings',
                component: () => import('./components/settings/profile/SettingsContainer.vue'),
            },
            {
                path: 'keys',
                name: 'keys',
                component: () => import('./components/settings/keys/SettingsContainer.vue'),
            },
        ],
    },
  ],
});
