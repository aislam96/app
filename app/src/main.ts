import Vue from 'vue';
import Kelp from './Kelp.vue';
import router from '@/router';
import store from '@/store';

import { auth } from './core/AppConfig';
import { retrievePrivateKey } from '@/core/Cryptography';

Vue.config.productionTip = false;

auth.onAuthStateChanged(async (user) => {
    if (user) {
        store.commit('setUID', user.uid);
        const privateKeyPackage = await retrievePrivateKey(user.uid).catch((err) => {
            console.log('Something went wrong getting private key');
        });
        if (privateKeyPackage) {
            store.commit('privateKeyFound', true);
        } else {
            store.commit('privateKeyFound', false);
        }
    } else {
        store.commit('loggedIn', false);
        store.commit('privateKeyFound', false);
    }
    new Vue({
        router,
        store,
        render: (h) => h(Kelp),
    }).$mount('#Kelp');
});
