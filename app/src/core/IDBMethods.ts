/**
 * DO NOT CACHE ANY DATA THAT IS ENCRYPTED IN A DECRYPTED STATE.
 * SAVE ENCRYPTED DATA ALONG WITH NONCE.
 * DO NOT SAVE SHARED KEYS OR ANY OTHER SYMMETRIC KEYS.
 *
 * This whole package is to local caching. In the absence of cached data
 * make sure to retrieve everything from the cloud. And save locally afterwards.
 * Can't afford thousands of redundant database calls.
 */

import {
    idbPosts,
    idbPublicKey,
    idbUserInfo,
    IPost,
    IUser,
} from './AppConfig';
import {
    arrayToString,
} from './Cryptography';

/**
 *
 * @param post Post data.
 */
export async function savePostLocally(post: IPost) {
    await idbPosts.setItem(post.id, post).catch((err) => {
        console.log('Something went wrong trying to save the post locally');
        return Promise.reject(err);
    });
}

/**
 *
 * @param id Post id. NOTE: not poster's UID.
 */
export async function getPostLocally(id: string) {
    let post: IPost;
    post = await idbPosts.getItem(id).catch((err) => {
        return Promise.reject(false);
    }) as IPost;
    return post;
}

/**
 *
 * @param user User info.
 */
export async function saveUserInfoLocally(user: IUser) {
    await idbUserInfo.setItem(user.uid, user).catch((err) => {
        console.log('Something went wrong trying to save the post locally');
        return Promise.reject(err);
    });
}

/**
 *
 * @param uid User's UID.
 */
export async function getUserInfoLocally(uid: string) {
    let user: IUser;
    user = await idbUserInfo.getItem(uid).catch((err) => {
        return Promise.reject(false);
    }) as IUser;
    return user;
}

/**
 *
 * @param uid User's UID.
 * @param publicKey Public key.
 */
export async function savePublicKeyLocally(uid: string, publicKey: string | Uint8Array) {
    if (publicKey instanceof Uint8Array) {
        publicKey = arrayToString(publicKey);
    }
    await idbPublicKey.setItem(uid, publicKey).catch((err) => {
        console.log('Something went wrong trying to save the post locally');
        return Promise.reject(err);
    });
}

/**
 *
 * @param uid User's UID.
 */
export async function getPublicKeyLocally(uid: string) {
    const publicKey = await idbPublicKey.getItem(uid).catch((err) => {
        return Promise.reject(false);
    }) as string;
    return publicKey;
}

