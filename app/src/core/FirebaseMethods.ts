import {
    arrayToString,
    IEncryptedPackage,
    stringToArray,
} from '@/core/Cryptography';
import {
    db,
    auth,
    IUser,
} from '@/core/AppConfig';
import {
    addPostSQL,
} from '@/core/SQLMethods';

export async function getToken() {
    const token = await auth.currentUser!.getIdToken(true).catch((err) => {
        console.log('Something went wrong trying to get token');
    });
    return token as string;
}

/**
 * Saves user ID and email to firebase for quick look up
 *
 * @export
 * @param {string} uid User's unique ID.
 * @param {string} email User's email.
 * @param {string} firstName User's first name.
 * @param {string} lastName User's last name.
 * @param {string} phone Optional phone number.
 * @returns Promise, resolved if successful.
 */
export async function saveUserInfo(uid: string, email: string, firstName: string, lastName: string, phone?: string) {
    const userRef = db.collection('Users').doc(uid);
    let info: IUser;
    if (phone) {
        info = {
            uid,
            email,
            firstName,
            lastName,
            phone,
        };
    } else {
        info = {
            uid,
            email,
            firstName,
            lastName,
            phone: '',
        };
    }
    await userRef.set(info).catch((err) => {
        console.log('Something went wrong trying to save user info.');
    });
}

/**
 * Retrieves user info given email address
 *
 * @param {string} email Email address of user.
 * @returns Promise containing query results.
 */
export async function getUserByUID(uid: string) {
    const userSnapShot = await db.collection('Users').doc(uid).get().catch((err) => {
        console.log('Something went wrong while getting user using ', uid);
        return Promise.reject(err);
    });

    if (userSnapShot.exists) {
        const data = userSnapShot.data();
        const user: IUser = {
            firstName: data!.firstName as string,
            lastName: data!.lastName as string,
            uid: data!.uid as string,
            email: data!.email as string,
        };
        return user;
    } else {
        return Promise.reject(new Error('Could not find the user.'));
    }
}

/**
 * Retrieves user info given email address
 *
 * @param {string} email Email address of user.
 * @returns Promise containing query results.
 */
export async function getUserByEmail(email: string) {
    const userSnapShot = await db.collection('Users').where('email', '==', email).get().catch((err) => {
        console.log('Something went wrong while getting user using', email);
        return Promise.reject(err);
    });

    const users: IUser[] = [];

    userSnapShot.forEach((doc) => {
        const data = doc.data();
        const user: IUser = {
            firstName: data.firstName as string,
            lastName: data.lastName as string,
            uid: data.uid as string,
            email: data.email as string,
        };
        users.push(user);
    });

    if (users.length > 0) {
        return users;
    } else {
        return Promise.reject(new Error('Could not find the user.'));
    }
}

/**
 * Retrieves user info given phone number.
 *
 * @export
 * @param {string} phone Email address of user.
 * @returns Promise containing query results.
 */
export async function getUserByPhone(phone: string) {
    const userSnapShot = await db.collection('Users').where('phone', '==', phone).get().catch((err) => {
        console.log('Something went wrong while getting user using', phone);
        return Promise.reject(err);
    });

    const users: IUser[] = [];

    userSnapShot.forEach((doc) => {
        const data = doc.data();

        const user: IUser = {
            firstName: data.firstName as string,
            lastName: data.lastName as string,
            uid: data.uid as string,
            email: data.email as string,
        };
        users.push(user);
    });

    if (users.length > 0) {
        return users;
    } else {
        return Promise.reject(new Error('Could not find the user.'));
    }
}

/**
 * Saves my public key to firebase
 *
 * @export
 * @param {string} myUid My user ID.
 * @param {(Uint8Array | string)} publicKey My public key.
 * @returns Promise, resolved if the operation was successful.
 */
export async function savePublicAndLocalSymmetricKey(
    myUid: string, publicKey: Uint8Array | string, localSymmetricKey: Uint8Array | string) {

    const myKeyRef = db.collection('Keys').doc(myUid);

    const entry: any = {};
    if (publicKey instanceof Uint8Array) {
        entry.publicKey = arrayToString(publicKey);
    } else {
        entry.publicKey = publicKey;
    }

    if (localSymmetricKey instanceof Uint8Array) {
        entry.localSymmetricKey = arrayToString(localSymmetricKey);
    } else {
        entry.localSymmetricKey = localSymmetricKey;
    }

    await myKeyRef.set(entry).catch((err) => {
        console.log('Something went wrong trying to save public and local symmetric keys.');
    });
}

/**
 * Retrieves public key from firestore given uid.
 *
 * @export
 * @param {string} uid My uid
 * @returns Promise containing query results
 */
export async function getPublicAndSymmetricKey(uid: string) {
    const publicKeyRef = db.collection('Keys').doc(uid);

    const keyDoc = await publicKeyRef.get().catch((err) => {
            console.log('Something went wrong getting public and local symmetric keys.');
            return Promise.reject(err);
        });

    if (keyDoc.exists) {
        return keyDoc.data() as any;
    } else {
        return Promise.reject(new Error('Could not find the document containing public key'));
    }
}

/**
 * Retrieves public key from firestore given uid.
 *
 * @export
 * @param {string} uid My uid
 * @returns Promise containing query results
 */
export async function getPublicKey(uid: string): Promise <string> {
    const data = await getPublicAndSymmetricKey(uid).catch((err) => {
        console.log('Something went wrong getting public key.');
        return Promise.reject(err);
    }) as any;
    return data!.publicKey as string;
}

/**
 * Retrieves public key from firestore given uid.
 *
 * @export
 * @param {string} uid My uid
 * @returns Promise containing query results
 */
export async function getLocalSymmetricKey(uid: string): Promise <string> {
    const data = await getPublicAndSymmetricKey(uid).catch((err) => {
        console.log('Something went wrong getting local symmetric key.');
        return Promise.reject(err);
    }) as any;
    return data!.localSymmetricKey as string;
}

/**
 * Saves my shared key encrypted with friend"s public key to friend"s document
 *
 * @export
 * @param {string} myUid My user ID
 * @param {string} friendUid Friend"s user ID
 * @param {Uint8Array} sharedKey My shared key encrypted using friend"s public key
 * @returns Promise, resolved if the operation was successful
 */
export async function saveSharedKey(myUid: string, friendUid: string, sharedKeyPackage: IEncryptedPackage) {
    const baseSharedKeyRef = db.collection('SharedKeys').doc('SharedKeys');
    const friendSharedKeyRef = baseSharedKeyRef.collection(friendUid).doc(myUid);

    const entry: any = {};

    entry.uid = myUid;
    entry.encryptedKey = arrayToString(sharedKeyPackage.encryptedData);
    entry.nonce = arrayToString(sharedKeyPackage.nonce);

    await friendSharedKeyRef.set(entry).catch((err) => {
        console.log('Something went wrong trying to save shared key.');
    });
}

/**
 * Retrieves friend's shared key that was encrypted using my public, and friend"s private key along with nonce
 *
 * @export
 * @param {string} myUid My user ID
 * @param {string} friendUid Friend"s user ID
 * @returns Promise containing query results
 */
export async function getSharedKey(myUid: string, friendUid: string) {
    const baseSharedKeyRef = db.collection('SharedKeys').doc('SharedKeys');
    const mySharedKeyRef = baseSharedKeyRef.collection(myUid).doc(friendUid);

    const friendSharedKeyDoc = await mySharedKeyRef.get().catch((err) => {
        console.log('Something went wrong getting shared key.');
        console.log(err);
        return Promise.reject(err);
    });

    if (!friendSharedKeyDoc.exists) {
        return Promise.reject(new Error('Document containing friend\'s shared key was not found'));
    }
    const encryptedKey = friendSharedKeyDoc.data() !.encryptedKey;
    const nonce = friendSharedKeyDoc.data() !.nonce;
    const encryptedDataPackage: IEncryptedPackage = {
        encryptedData: stringToArray(encryptedKey),
        nonce: stringToArray(nonce),
    };

    return encryptedDataPackage;
}

/**
 * Deletes shared keys between two users.
 *
 * @export
 * @param {string} myUid My user ID
 * @param {string} friendUid Friend"s user ID
 * @returns true if deletion was successful.
 */
export async function deleteSharedKey(myUid: string, friendUid: string) {
    const baseSharedKeyRef = db.collection('SharedKeys').doc('SharedKeys');

    await baseSharedKeyRef.collection(friendUid).doc(myUid).delete().catch((err) => {
        console.log('Something went wrong deleting my key from friend\'s document.');
    });
    await baseSharedKeyRef.collection(myUid).doc(friendUid).delete().catch((err) => {
        console.log('Something went wrong deleting friend\'s key from my document.');
    });
    return true;
}

export async function saveEncryptedPost(uid: string, encryptedData: IEncryptedPackage) {
    const postRef = db.collection('Posts');
    const data: any = {};

    data.uid = uid;
    data.data = arrayToString(encryptedData.encryptedData);
    data.nonce = arrayToString(encryptedData.nonce);
    data.date = new Date().toISOString().slice(0, 19).replace('T', ' ');

    const ref = await postRef.add(data).catch((err) => {
        console.log('Something went wrong trying to save encrypted post.');
    }) as firebase.firestore.DocumentReference;
    const postId = ref.id;
    const postDoc = await ref.get();
    if (postDoc.exists) {
        await addPostSQL(postId, uid, data.date).catch((err) => {
            console.log('Couldn\'t save post to SQL database.');
            console.log(err);
        });
    }
    return postId;
}

export function getEncryptedPost(postID: string) {
    const query = db.collection('Posts').doc(postID);
    return query.get();
}

