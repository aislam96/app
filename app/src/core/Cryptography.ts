import * as nacl from 'tweetnacl';
import {
    idbPrivateKey,
    idbPublicKey,
} from './AppConfig';
import {
    getPublicKey,
} from './FirebaseMethods';
import { getPublicKeyLocally } from '@/core/IDBMethods';

/** Packages encrypted data along with a nonce. Both fields are Uint8Array */
export interface IEncryptedPackage {
    encryptedData: Uint8Array;
    nonce: Uint8Array;
}

/**
 *
 * Generates randomly generated shared key used for symmetric encryption
 * @export
 * @returns {Uint8Array} Shared key
 */
export function generateSharedKey(): Uint8Array {
    return nacl.randomBytes(nacl.secretbox.keyLength);
}

/**
 *
 * Generates a public and secret key pair for asymmetric encryption
 * @export
 * @returns {nacl.BoxKeyPair} Key pair object
 */
export function generateKeyPair(): nacl.BoxKeyPair {
    return nacl.box.keyPair();
}

/**
 *
 * Encrypts a shared key using my private key and my friend's public key
 * @export
 * @param {Uint8Array} SharedKey Shared key to be encrypted
 * @param {Uint8Array} theirPublicKey Friend's public key
 * @returns {IEncryptedPackage} Encrypted shared key and nonce
 */
export function encryptSharedKey(
    sharedKey: Uint8Array, theirPublicKey: Uint8Array, myPrivateKey: Uint8Array): IEncryptedPackage {

    const nonce = nacl.randomBytes(nacl.secretbox.nonceLength);
    const encryptedSharedKey = nacl.box(sharedKey, nonce, theirPublicKey, myPrivateKey);

    return {
        encryptedData: encryptedSharedKey,
        nonce,
    };
}

/**
 *
 * Decrypts a shared key using my private key and my friend's public key
 * @export
 * @param {IEncryptedPackage} encryptedSharedKeyPackage Shared key to be decrypted
 * @param {Uint8Array} theirPublicKey My public key
 * @param {Uint8Array} myPrivateKey My private key
 * @returns {Uint8Array} Decrypted shared key or null
 */
export function decryptSharedKey(encryptedSharedKeyPackage: IEncryptedPackage,
                                 friendPublicKey: Uint8Array, myPrivateKey: Uint8Array): Uint8Array | null {

    const nonce = encryptedSharedKeyPackage.nonce;
    const encryptedSharedKey = encryptedSharedKeyPackage.encryptedData;

    const decryptedSharedKey: Uint8Array | null = nacl.box.open(encryptedSharedKey,
        nonce, friendPublicKey, myPrivateKey);

    return decryptedSharedKey;
}

/**
 *
 * Encrypts some given data using a shared key
 * @export
 * @param {Uint8Array} data Data to be encrypted
 * @param {Uint8Array} sharedKey Shared key used for symmetric encryption
 * @returns {IEncryptedPackage} Encrypted data and nonce
 */
export function encryptData(data: Uint8Array, sharedKey: Uint8Array): IEncryptedPackage {
    const nonce = nacl.randomBytes(nacl.secretbox.nonceLength);
    const encryptedData = nacl.secretbox(data, nonce, sharedKey);

    return {
        encryptedData,
        nonce,
    };
}

/**
 *
 * Decrypts some data using a shared key
 * @export
 * @param {IEncryptedPackage} dataPackage Package containing encrypted data and nonce
 * @param {Uint8Array} sharedKey Shared key used for symmetric encryption
 * @returns {(Uint8Array | null)} Decrypted data or null
 */
export function decryptData(dataPackage: IEncryptedPackage, sharedKey: Uint8Array | string): Uint8Array | null {
    if (sharedKey instanceof Uint8Array) {
        sharedKey = sharedKey;
        const nonce = dataPackage.nonce;
        const encryptedData = dataPackage.encryptedData;
        const decryptedData = nacl.secretbox.open(encryptedData, nonce, sharedKey);
        return decryptedData;
    } else {
        sharedKey = stringToArray(sharedKey);
        const nonce = dataPackage.nonce;
        const encryptedData = dataPackage.encryptedData;
        const decryptedData = nacl.secretbox.open(encryptedData, nonce, sharedKey as Uint8Array);
        return decryptedData;
    }
}

/**
 *
 * Stores private key in local IndexedDB
 * @export
 * @param {string} uid User's ID
 * @param {string} privateKey User's private key
 */
export async function storePrivateKey(uid: string, encryptedPrivateKey: string, nonce: string) {
    await idbPrivateKey.setItem(uid, {
        encryptedPrivateKey,
        nonce,
    }).catch((err) => {
        console.log('Something went wrong when storing private key.');
        throw err;
    });
}

/**
 *
 * Retrieves a private key from local IndexedDB
 * @export
 * @param {string} uid User's ID
 * @returns {Promise<IEncryptedPackage>} Resolves if user's private key was found
 */
export async function retrievePrivateKey(uid: string): Promise < IEncryptedPackage > {

    const result = await (idbPrivateKey.getItem(uid) as any).catch((err: Error) => {
        console.log('Something went wrong when getting private key from indexedDB.');
        throw err;
    });

    const encryptedPrivateKey = stringToArray(result.encryptedPrivateKey);
    const nonce = stringToArray(result.nonce);

    return {
        encryptedData: encryptedPrivateKey,
        nonce,
    };
}

export async function retrievePublicKey(uid: string, forceDownload?: boolean) {
    let publicKey: string;
    if (forceDownload) {
        publicKey = await getPublicKey(uid).catch((err) => {
            console.log('Something went wrong trying to download public key.');
        }) as string;
    } else {
        publicKey = await getPublicKeyLocally(uid).catch((err) => {
            console.log('Something went wrong getting private key locally, will download.');
        }) as string;
    }
    if (!publicKey) {
        publicKey = await getPublicKey(uid).catch((err) => {
            console.log('Something went wrong trying to download public key.');
        }) as string;
    } else {
        return publicKey;
    }
}

/**
 *
 * Converts an Uint8Array to string
 * @export
 * @param {Uint8Array} arr Array to be converted
 * @returns {string} Converted string
 */
export function arrayToString(arr: Uint8Array): string {
    return arr.toString();
}

/**
 *
 * Converts a string that was created using arrayToString() back to Uint8Array
 * @export
 * @param {string} str String to be converted
 * @returns {Uint8Array} Converted Uint8Array
 */
export function stringToArray(str: string): Uint8Array {
    const arrStr: string[] = str.split(',');
    const arrNum: number[] = arrStr.map(Number);

    const uint8: Uint8Array = new Uint8Array(arrNum);

    return uint8;
}
