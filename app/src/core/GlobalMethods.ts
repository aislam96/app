import {
    getUserInfoLocally,
    saveUserInfoLocally,
} from './IDBMethods';
import {
    getUserByUID,
} from './FirebaseMethods';
import {
    IUser,
} from './AppConfig';

export async function getUserInfo(uid: string, forceDownload?: boolean) {
    let userInfo!: IUser;
    if (!forceDownload) {
        userInfo = await getUserInfoLocally(uid);
    }
    if (!userInfo) {
        userInfo = await getUserByUID(uid).catch((err) => {
            console.log('Something went wrong getting user\'s info.');
        }) as IUser;
        await saveUserInfoLocally(userInfo);
    }
    return userInfo;
}
