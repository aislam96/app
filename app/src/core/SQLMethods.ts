import { getToken } from '@/core/FirebaseMethods';
import { auth } from '@/core/AppConfig';

const url = 'http://localhost:8070/api/';
// const url = 'https://ai-yb-prototype.appspot.com/api/';

const header = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Token': '',
};

/**
 * Adds friend to SQL database
 * @param uid My UID
 * @param friendUid Friend's UID
 */
export async function addFriendSQL(uid: string, friendUid: string) {
    const theHeader = header;
    theHeader.Token = await getToken().catch((err) => {
        console.log('Something went wrong getting token');
    }) as string;

    const response = await fetch(url + 'addfriend', {
        mode: 'cors',
        headers: theHeader,
        method: 'PUT',
        body: JSON.stringify({
            MyUID: uid,
            FriendUID: friendUid,
        }),
    }).catch((err) => {
        console.log('Something went wrong fetching response.');
        console.log(err);
    }) as Response;

    if (response.status === 200) {
        return Promise.resolve(true);
    } else {
        return Promise.reject(false);
    }
}

/**
 * Adds friend to SQL database
 * @param uid My UID
 * @param friendUid Friend's UID
 */
export async function checkFriendSQL(uid: string, friendUid: string) {
    const theHeader = header;
    theHeader.Token = await getToken().catch((err) => {
        console.log('Something went wrong getting token');
    }) as string;

    const response = await fetch(url + 'checkfriend', {
        mode: 'cors',
        headers: theHeader,
        method: 'POST',
        body: JSON.stringify({
            MyUID: uid,
            FriendUID: friendUid,
        }),
    }).catch((err) => {
        console.log('Something went wrong fetching response.');
        console.log(err);
    }) as Response;

    const data = await response.json().catch((err) => {
        console.log('Trouble parsing JSON');
        console.log(err);
    });
    return data;
}

/**
 * Deletes friend in SQL database
 * @param uid My UID
 * @param friendUid Friend's UID
 */
export async function deleteFriendSQL(uid: string, friendUid: string) {
    const theHeader = header;
    theHeader.Token = await getToken().catch((err) => {
        console.log('Something went wrong getting token');
    }) as string;

    const response = await fetch(url + 'deletefriend', {
        mode: 'cors',
        headers: theHeader,
        method: 'DELETE',
        body: JSON.stringify({
            MyUID: uid,
            FriendUID: friendUid,
        }),
    }).catch((err) => {
        console.log('Something went wrong fetching response.');
        console.log(err);
    }) as Response;

    if (response.status === 200) {
        return Promise.resolve(true);
    } else {
        return Promise.reject(false);
    }
}

export async function addPostSQL(postId: string, uid: string, time: string) {
    const theHeader = header;
    theHeader.Token = await getToken().catch((err) => {
        console.log('Something went wrong getting token');
    }) as string;

    const response = await fetch(url + 'addpost', {
        mode: 'cors',
        method: 'PUT',
        headers: theHeader,
        body: JSON.stringify({
            ID: postId,
            MyUID: uid,
            Time: time,
        }),
    });
    if (response.status === 200) {
        return Promise.resolve(true);
    } else {
        return Promise.reject(false);
    }
}

export async function getPostsSQL(uid: string, offset: number) {
    const theHeader = header;
    theHeader.Token = await getToken().catch((err) => {
        console.log('Something went wrong getting token');
    }) as string;

    const response = await fetch(url + 'getposts', {
        method: 'POST',
        mode: 'cors',
        headers: theHeader,
        body: JSON.stringify({
            MyUID: uid,
            Offset: offset,
        }),
    }).catch((err) => {
        console.log('Something went wrong fetching response.');
        console.log(err);
    }) as Response;

    const data = await response.json().catch((err) => {
        console.log('Trouble parsing JSON');
        console.log(err);
    });
    return data;
}

export async function getUserPostsSQL(uid: string, offset: number) {
    const theHeader = header;
    theHeader.Token = await getToken().catch((err) => {
        console.log('Something went wrong getting token');
        console.log(err);
    }) as string;

    const response = await fetch(url + 'getuserposts', {
        method: 'POST',
        mode: 'cors',
        headers: theHeader,
        body: JSON.stringify({
            MyUID: uid,
            Offset: offset,
        }),
    }).catch((err) => {
        console.log('Something went wrong fetching response.');
        console.log(err);
    }) as Response;

    const data = await response.json();
    return data;
}
