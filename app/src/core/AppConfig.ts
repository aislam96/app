import * as fb from 'firebase';
import * as localforage from 'localforage';

const appName = 'Kelp';

const appConfig = {
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    messagingSenderId: '',
    projectId: '',
    storageBucket: '',
};

const idbConfigPrivateKey = {
        driver      : localforage.INDEXEDDB,
        name        : appName,
        version     : 1,
        storeName   : 'PrivateKeys',
        description : 'Users\' private keys stored locally',
};

const idbConfigPublicKey = {
    driver      : localforage.INDEXEDDB,
    name        : appName,
    version     : 1,
    storeName   : 'PublicKeys',
    description : 'Users\' private keys stored locally',
};

const idbConfigPosts = {
    driver      : localforage.INDEXEDDB,
    name        : appName,
    version     : 1,
    storeName   : 'Posts',
    description : 'Users\' encrypted posts stored locally',
};

const idbConfigUserInfo = {
    driver      : localforage.INDEXEDDB,
    name        : appName,
    version     : 1,
    storeName   : 'UserInfo',
    description : 'Users\' information stored locally',
};

export interface IPost {
    uid?: string;
    id: string;
    firstName: string;
    lastName: string;
    date: string;
    content: string;
    nonce?: string;
}

export interface IUser {
    uid: string;
    email: string;
    phone?: string;
    firstName: string;
    lastName: string;
}

export const idbPrivateKey: LocalForage = localforage.createInstance(idbConfigPrivateKey);
export const idbPublicKey: LocalForage = localforage.createInstance(idbConfigPublicKey);
export const idbPosts: LocalForage = localforage.createInstance(idbConfigPosts);
export const idbUserInfo: LocalForage = localforage.createInstance(idbConfigUserInfo);

export const app: fb.app.App = fb.initializeApp(appConfig);
export const db: fb.firestore.Firestore = app.firestore();
export const auth: fb.auth.Auth = app.auth();
