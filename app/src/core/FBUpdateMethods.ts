import * as firebase from 'firebase';
import {
    db,
    auth,
} from '@/core/AppConfig';

export async function changeName(uid: string, firstName: string, lastName: string) {
    const userRef = db.collection('Users').doc(uid);
    const nameObject = {
        firstName,
        lastName,
    };
    await userRef.update(nameObject).catch((err) => {
        console.log('Couldn\'t change name');
    });
}

/**
 * @param user `User` object containing user's credentials.
 * @param newEmail new email address.
 */
export async function changeEmail(user: firebase.User, newEmail: string) {
    let caughtException = false;
    let errorString = '';
    await user.updateEmail(newEmail).catch((err) => {
        caughtException = true;
        switch (err.code.toString()) {
            case 'auth/invalid-email':
                errorString = 'The email address is invalid.';
                break;
            case 'auth/requires-recent-login':
                errorString = 'Please try loggin in again and retrying the operation.';
                break;
            case 'auth/email-already-in-use':
                errorString = 'Email address is already in use.';
                break;
            default:
                errorString = 'Could not fulfill the request.';
                break;
        }
    });

    if (caughtException) {
        return Promise.reject(errorString);
    }

    await changeEmailUserInfo(user.uid, newEmail);
}

export async function changeEmailUserInfo(uid: string, email: string) {
    const userRef = db.collection('Users').doc(uid);
    const emailObject = {
        email,
    };
    await userRef.update(emailObject).catch((err) => {
        console.log('Couldn\'t change email in user info.');
    });
}

/**
 * @param email user's email
 * @param password user's password
 * @returns promise containing `User` object or rejects to `false` if verification failed.
 */
export async function verifyPassword(email: string, password: string) {
    let noException = true;
    const credentials = firebase.auth.EmailAuthProvider.credential(email, password);
    const user = auth.currentUser as firebase.User;
    await user.reauthenticateAndRetrieveDataWithCredential(credentials).catch((err) => {
        noException = false;
    });

    if (noException) {
        return user;
    } else {
        return Promise.reject(noException);
    }
}

/**
 * Retrieves user info given email address
 *
 * @param {string} email Email address of user.
 * @returns Promise containing query results.
 */
export async function checkUserByEmail(email: string) {
    let userExist = false;
    const userSnapShot = await db.collection('Users').where('email', '==', email).get().catch((err) => {
        console.log('Something went wrong while getting user using', email);
    }) as firebase.firestore.QuerySnapshot;

    userSnapShot.forEach((doc: any) => {
        userExist = true; // at least one user exists with the corresponding email address.
        return;
    });

    return userExist;
}

export async function changePassword(password: string) {
    let noException = true;
    let errMsg = '';
    const user = auth.currentUser as firebase.User;
    await user.updatePassword(password).catch((err) => {
        noException = false;
        errMsg = err.code.toString();
    });
    if (!noException) {
        return Promise.reject(errMsg);
    }
}
