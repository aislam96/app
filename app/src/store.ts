import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        uid: '',
        loggedIn: false,
        hasPrivateKey: false,
    },
    mutations: {
        setUID(state, uid: string) {
            state.uid = uid;
            state.loggedIn = true;
        },
        logOut(state) {
            state.uid = '';
            state.loggedIn = false;
        },
        privateKeyFound(state, isFound: boolean) {
            state.hasPrivateKey = isFound;
        },
        // toggleLoggedOut(state) {
        //     state.loggedOut = true;
        //     state.loggedIn = false;
        // },
    },
    actions: {

    },
});
