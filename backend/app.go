package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	"google.golang.org/appengine"
)

// FriendStruct used to store friend info
type FriendStruct struct {
	MyUID     string
	FriendUID string
}

// CheckFriendStruct used to store friend info
type CheckFriendStruct struct {
	AddedMe   bool
	AddedByMe bool
}

// SavePostStruct used to store post info
type SavePostStruct struct {
	ID    string
	MyUID string
	Time  string
}

// GetPostStruct used to store post retrieval info
type GetPostStruct struct {
	MyUID  string
	Offset int
}

var db *sql.DB

var addFriendStatement *sql.Stmt
var addPostStatement *sql.Stmt
var getPostsStatement *sql.Stmt
var checkFriendStatement *sql.Stmt
var deleteFriendStatement *sql.Stmt
var getUserPostsStatement *sql.Stmt

var app *firebase.App

// MAKE SURE TO SET THIS BEFORE PUBLISHING.
var isProduction = false

func main() {
	app = initApp()
	if isProduction {
		/* START PRODUCTION DB CONFIG HERE */
		var (
			connectionName = mustGetenv("CLOUDSQL_CONNECTION_NAME")
			user           = mustGetenv("CLOUDSQL_USER")
			password       = mustGetenv("CLOUDSQL_PASSWORD")
			dbName         = mustGetenv("CLOUDSQL_TEST_DB_NAME") // always use test DB until actual shipment.
		)
		var err error
		db, err = sql.Open("mysql", fmt.Sprintf("%s:%s@cloudsql(%s)/%s", user, password, connectionName, dbName))
		if err != nil {
			log.Fatalf("Could not open db: %v", err)
		}
		/* END PRODUCTION DB CONFIG HERE */
		initHandlers()
		appengine.Main()

	} else {
		var err error
		db, err = sql.Open("mysql", "abdullah:password123@tcp([kelp.c9ukpq5hef6u.us-east-2.rds.amazonaws.com]:3306)/Kombu")
		if err != nil {
			log.Fatalf("Could not open db: %v", err)
		}
		initHandlers()
		if err := http.ListenAndServe(":8070", nil); err != nil {
			panic(err)
		}
	}
	defer getPostsStatement.Close()
	defer addFriendStatement.Close()
	defer addPostStatement.Close()
}

func initHandlers() {
	prepareQueries()

	fs := http.FileServer(http.Dir("./www"))
	http.Handle("/assets/", http.StripPrefix("/assets/", fs))

	http.HandleFunc("/", root)

	// {"MyUID": "<MyUID>", "FriendUID": "<friend's UID>"}
	http.HandleFunc("/api/addfriend", addFriend)

	// {"MyUID": "<MyUID>", "FriendUID": "<friend's UID>"}
	http.HandleFunc("/api/checkfriend", checkFriend)

	// {"MyUID": "<MyUID>", "FriendUID": "<friend's UID>"}
	http.HandleFunc("/api/deletefriend", deleteFriend)

	// {"ID": "<post id>", "MyUID": "<MyUID>", "Time": "<time>"}
	http.HandleFunc("/api/addpost", addPost)

	// {"MyUID": "<MyUID>", "Offset": "<offset>"}
	http.HandleFunc("/api/getposts", getPosts)

	// {"UID": "<UID>", "Offset": "<offset>"}
	http.HandleFunc("/api/getuserposts", getUserPosts)
}

func handleCORS(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
	(*w).Header().Set("Access-Control-Allow-Methods", "PUT, OPTIONS, POST, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Token")
}

func root(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	w.Write([]byte("Hello"))
}

func addFriend(w http.ResponseWriter, r *http.Request) {
	handleCORS(&w, r)
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		return
	} else if r.Method != "PUT" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	var friend FriendStruct
	decoder := json.NewDecoder(r.Body)
	friendParseErr := decoder.Decode(&friend)
	if friendParseErr != nil {
		w.WriteHeader(500)
		log.Panic(friendParseErr)
		return
	}

	token := verifyIDToken(r.Header.Get("Token"))

	var myUID = friend.MyUID
	var friendUID = friend.FriendUID

	if myUID != token.UID {
		w.WriteHeader(403)
		w.Write([]byte("403 - Forbidden"))
		return
	}

	if myUID == "" || friendUID == "" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	_, errInsert := addFriendStatement.Exec(myUID, friendUID)
	if errInsert != nil {
		w.WriteHeader(500)
		w.Write([]byte("500 - Insertion Error"))
		log.Println(errInsert, myUID, friendUID)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("200 - Success"))
}

func checkFriend(w http.ResponseWriter, r *http.Request) {
	handleCORS(&w, r)
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		return
	} else if r.Method != "POST" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	var friend FriendStruct
	decoder := json.NewDecoder(r.Body)
	friendParseErr := decoder.Decode(&friend)
	if friendParseErr != nil {
		w.WriteHeader(500)
		log.Panic(friendParseErr)
		return
	}

	token := verifyIDToken(r.Header.Get("Token"))

	var myUID = friend.MyUID
	var friendUID = friend.FriendUID

	if myUID != token.UID {
		w.WriteHeader(403)
		w.Write([]byte("403 - Forbidden"))
		return
	}

	if myUID == "" || friendUID == "" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	addedByMeRow, errQuery := checkFriendStatement.Query(myUID, friendUID)
	if errQuery != nil {
		log.Panic(errQuery)
		return
	}

	defer addedByMeRow.Close()
	var check CheckFriendStruct
	for addedByMeRow.Next() {
		if err := addedByMeRow.Scan(&check.AddedByMe); err != nil {
			log.Fatal(err)
		}
	}

	addedMeRow, errQuery := checkFriendStatement.Query(friendUID, myUID)
	if errQuery != nil {
		log.Panic(errQuery)
		return
	}
	defer addedMeRow.Close()
	for addedMeRow.Next() {
		if err := addedMeRow.Scan(&check.AddedMe); err != nil {
			log.Panic(err)
		}
	}

	jData, errJSON := json.Marshal(check)
	// // for debugging.
	// sData := string(jData[:])
	// log.Printf(sData)
	if errJSON != nil {
		log.Panic(errJSON, jData)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

func deleteFriend(w http.ResponseWriter, r *http.Request) {
	handleCORS(&w, r)
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		return
	} else if r.Method != "DELETE" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	var friend FriendStruct
	decoder := json.NewDecoder(r.Body)
	friendParseErr := decoder.Decode(&friend)
	if friendParseErr != nil {
		w.WriteHeader(500)
		log.Panic(friendParseErr)
		return
	}

	token := verifyIDToken(r.Header.Get("Token"))

	var myUID = friend.MyUID
	var friendUID = friend.FriendUID

	if myUID != token.UID {
		w.WriteHeader(403)
		w.Write([]byte("403 - Forbidden"))
		return
	}

	if myUID == "" || friendUID == "" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	_, errInsert := deleteFriendStatement.Exec(myUID, friendUID, friendUID, myUID)
	if errInsert != nil {
		w.WriteHeader(500)
		w.Write([]byte("500 - Insertion Error"))
		log.Println(errInsert, myUID, friendUID)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("200 - Success"))
}

func addPost(w http.ResponseWriter, r *http.Request) {
	handleCORS(&w, r)
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		return
	} else if r.Method != "PUT" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	var post SavePostStruct
	decoder := json.NewDecoder(r.Body)
	postParseErr := decoder.Decode(&post)
	if postParseErr != nil {
		w.WriteHeader(400)
		log.Println(postParseErr)
		return
	}

	token := verifyIDToken(r.Header.Get("Token"))

	var id = post.ID
	var uid = post.MyUID
	var time = post.Time

	if uid != token.UID {
		w.WriteHeader(403)
		w.Write([]byte("403 - Forbidden"))
		return
	}

	if id == "" || uid == "" || time == "" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	_, errInsert := addPostStatement.Exec(id, uid, time)
	if errInsert != nil {
		log.Println(errInsert, id, uid, time)
		w.WriteHeader(500)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("200 - Success"))
}

func getPosts(w http.ResponseWriter, r *http.Request) {
	handleCORS(&w, r)
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		return
	} else if r.Method != "POST" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	var getPost GetPostStruct
	decoder := json.NewDecoder(r.Body)
	getPostParseErr := decoder.Decode(&getPost)
	if getPostParseErr != nil {
		log.Println(getPostParseErr)
	}

	token := verifyIDToken(r.Header.Get("Token"))

	uid := getPost.MyUID
	offset := getPost.Offset

	if uid != token.UID {
		w.WriteHeader(403)
		w.Write([]byte("403 - Forbidden"))
		return
	}

	if uid == "" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	rows, errQuery := getPostsStatement.Query(uid, uid, offset*100)
	if errQuery != nil {
		log.Panic(errQuery, uid, offset)
		w.WriteHeader(500)
		return
	}
	defer rows.Close()

	postIDs := make([]string, 0)
	for rows.Next() {
		var postID string
		if err := rows.Scan(&postID); err != nil {
			log.Panic(err)
		}
		postIDs = append(postIDs, postID)
	}

	jData, errJSON := json.Marshal(postIDs)
	// // for debugging.
	// sData := string(jData[:])
	// log.Printf(sData)
	if errJSON != nil {
		log.Panic(errJSON, jData)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

func getUserPosts(w http.ResponseWriter, r *http.Request) {
	handleCORS(&w, r)
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		return
	} else if r.Method != "POST" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	var getPost GetPostStruct
	decoder := json.NewDecoder(r.Body)
	getPostParseErr := decoder.Decode(&getPost)
	if getPostParseErr != nil {
		log.Println(getPostParseErr)
	}

	token := verifyIDToken(r.Header.Get("Token"))

	uid := getPost.MyUID
	offset := getPost.Offset

	if uid != token.UID {
		w.WriteHeader(403)
		w.Write([]byte("403 - Forbidden"))
		return
	}

	if uid == "" {
		w.WriteHeader(404)
		w.Write([]byte("404 - NOT FOUND"))
		return
	}

	rows, errQuery := getUserPostsStatement.Query(uid, offset*100)
	if errQuery != nil {
		log.Panic(errQuery, uid, offset)
		w.WriteHeader(500)
		return
	}
	defer rows.Close()

	postIDs := make([]string, 0)
	for rows.Next() {
		var postID string
		if err := rows.Scan(&postID); err != nil {
			log.Panic(err)
		}
		postIDs = append(postIDs, postID)
	}

	jData, errJSON := json.Marshal(postIDs)
	// // for debugging.
	// sData := string(jData[:])
	// log.Printf(sData)
	if errJSON != nil {
		log.Panic(errJSON, jData)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

// Prepares SQL queries to be executed.
func prepareQueries() {
	var addFriendStatementErr error
	addFriendStatement, addFriendStatementErr = db.Prepare(`INSERT INTO
															Friends VALUES (?, ?);`)
	if addFriendStatementErr != nil {
		log.Fatalf("Could not prepare \"add friend\" statement: %v", addFriendStatementErr)
	}

	var addPostStatementErr error
	addPostStatement, addPostStatementErr = db.Prepare(`INSERT INTO
														Posts VALUES (?, ?, ?);`)
	if addPostStatementErr != nil {
		log.Fatalf("Could not prepare \"add post\" statement: %v", addPostStatementErr)
	}

	var getPostsStatementErr error
	getPostsStatement, getPostsStatementErr = db.Prepare(`SELECT id
														  FROM Posts
														  WHERE uid in (
														  		SELECT friend_uid
																FROM Friends
																WHERE uid=? AND uid IN (
																	SELECT uid
																	FROM Friends
																	WHERE friend_uid=?))
														  ORDER BY time DESC
														  LIMIT ?, 100;`)
	if getPostsStatementErr != nil {
		log.Fatalf("Could not prepare \"get post\" statement: %v", getPostsStatementErr)
	}

	var checkFriendStatementError error
	checkFriendStatement, checkFriendStatementError = db.Prepare(`SELECT TRUE
																  FROM Friends
																  WHERE (uid=? AND friend_uid=?);`)
	if checkFriendStatementError != nil {
		log.Fatalf("Could not prepare \"check friend\" statement: %v", addPostStatementErr)
	}

	var deleteFriendStatementError error
	deleteFriendStatement, checkFriendStatementError = db.Prepare(`DELETE
																  FROM Friends
																  WHERE ((uid=? AND friend_uid=?) OR 
																  (uid=? AND friend_uid=?));`)
	if deleteFriendStatementError != nil {
		log.Fatalf("Could not prepare \"check friend\" statement: %v", addPostStatementErr)
	}

	var getUserPostsStatementError error
	getUserPostsStatement, getUserPostsStatementError = db.Prepare(`SELECT id
																	FROM Posts
																	WHERE uid = ?
																	ORDER BY time DESC
																	LIMIT ?, 100;`)
	if getUserPostsStatementError != nil {
		log.Fatalf("Could not prepare \"check friend\" statement: %v", getUserPostsStatementError)
	}
}

func initApp() *firebase.App {
	opt := option.WithCredentialsFile("./credentials.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		log.Fatalf("error initializing app: %v", err)
	}

	return app
}

func verifyIDToken(idToken string) *auth.Token {
	client, err := app.Auth(context.Background())
	if err != nil {
		log.Panicf("error getting Auth client: %v\n", err)
	}

	token, err := client.VerifyIDToken(context.Background(), idToken)
	if err != nil {
		log.Panicf("error verifying ID token: %v\n", err)
	}
	// At this point token is verified.
	return token
}

// checks and gets required environment variables
func mustGetenv(k string) string {
	v := os.Getenv(k)
	if v == "" {
		log.Panicf("%s environment variable not set.", k)
	}
	return v
}
